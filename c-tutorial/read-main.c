#include <stdlib.h>
#include <stdio.h>


#include "readwrite.h"

int main() {
  float a = 0;
  float b = 0;
  float c = 0;

  printf("a = %f\nb = %f\nc = %f\n", a, b, c);

  // read my configuration
  read("input", &a, &b, &c);

  printf("a = %f\nb = %f\nc = %f\n", a, b, c);

  return EXIT_SUCCESS;
}
